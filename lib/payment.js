const Basichttp = require('basichttp');

module.exports = class PayMent extends Basichttp {
  constructor (host, port, protocol, username, password) {
    super();
    this.host = host;
    this.port = port;
    this.protocol = protocol;
    this.username = username;
    this.password = password;
    this.auth = 'Basic ' +  new Buffer(this.username + ':' + this.password).toString('base64');
  }
  
  /**
   * Update Authj  
   * @param {String <Token type>} token_type
   * @param {String <Acount Token>} value
   */
  updateAuth (token_type, value) {
    this.auth = token_type + ' ' + value;
  }

  /**
   * 
   * @param {state} 支付手段
   * @param {UUID} 支付法人id, payment id , orderid
   * 
   */
  _request ({state, UUID, data = '', method = 'POST'}) {
    let path = '';
    switch (state) {
      case 1: // 支付宝 消费者扫商家二维码
        path = '/commerce/payment_gateway/' + UUID + '/alipay/requestQRCode?_format=json';
        break;
      case 2: // 支付宝 商家扫消费者二维码
        path =  '/commerce/payment_gateway/' + UUID + '/alipay/capture?_format=json';
        break;
      case 3: // 微信 消费者扫商家二维码
        path =  '/commerce/payment_gateway/' + UUID + '/wechat_pay/requestQRCode?_format=json';
        break;
      case 4: // 微信 商家扫消费者二维码
        path =  '/commerce/payment_gateway/' + UUID + '/wechat_pay/capture?_format=json';
        break;
      case 5: // 支付宝沙盒 消费者扫商家二维码
        path =  '/commerce/payment_gateway/' + UUID + '/alipay_sandbox/requestQRCode?_format=json';
        break;
      case 6: // 支付宝沙盒 商家扫消费者二维码
        path =  '/commerce/payment_gateway/' + UUID + '/alipay_sandbox/capture?_format=json';
        break;
      case 7: // 微信沙盒 消费者扫商家二维码
        path =  '/commerce/payment_gateway/' + UUID + '/wechat_pay_sandbox/requestQRCode?_format=json';
        break;
      case 8: // 微信沙盒 商家扫消费者二维码
        path =  '/commerce/payment_gateway/' + UUID + '/wechat_pay_sandbox/capture?_format=json';
        break;
      case 9: // 支付宝 支付宝退款
        path =  '/commerce/payment_gateway/' + UUID + '/alipay/refund?_format=json';
        break;
      case 10: // 微信 微信退款
        path =  '/commerce/payment_gateway/' + UUID + '/wechat_pay/refund?_format=json';
        break;
      case 11: // 支付宝沙盒 支付宝沙盒退款
        path =  '/commerce/payment_gateway/' + UUID + '/alipay_sandbox/refund?_format=json';
        break;
      case 12: // 微信沙盒 微信沙盒退款
        path =  '/commerce/payment_gateway/' + UUID + '/wechat_pay_sandbox/refund?_format=json';
        break;
      case 13: // 微信 微信更新远程状态
        path =  '/commerce/payment_gateway/' + UUID + '/wechat_pay/checkRemoteState?_format=json';
        break;
      case 14: // 微信沙盒 微信沙盒更新远程状态
        path =  '/commerce/payment_gateway/' + UUID + '/wechat_pay_sandbox/checkRemoteState?_format=json';
        break;
      case 15: // 使用Payment UUID来查询Payment
        path =  '/jsonapi/commerce_payment/payment_default/' + UUID;
        break;
      case 16: // 使用Order UUID来查询Payment
        path =  '/jsonapi/commerce_payment/payment_default?filter[field_oc_order_uuid][value]=' + UUID;
        break;
      case 17: // 查询商家支持的支付手段
        path =  '/jsonapi/node/account/' + UUID;
        break;
      case 18: // 支付宝取消订单
        path = '/commerce/payment_gateway/' + UUID + '/alipay/cancel/?_format=json';
        break;
      case 19: // 支付宝沙盒取消订单
        path = '/commerce/payment_gateway/' + UUID + '/alipay_sandbox/cancel/?_format=json';
        break;
      case 20: // 微信取消订单
        path = '/commerce/payment_gateway/' + UUID + '/wechat_pay/cancel/?_format=json';
        break;
      case 21: // 微信沙盒取消订单
        path = '/commerce/payment_gateway/' + UUID + '/wechat_pay_sandbox/cancel/?_format=json';
        break;
      case 22: // 用于获取payment支付手段
        path = 'jsonapi/node/account/' + UUID;
        break;
      default:
        break;
    }

    let that = this;
    let options = {
      host: that.host,
      method: method,
      protocol: that.protocol,
      path: path,
      port: that.port,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': that.auth
      },
      data: data
    };

    let promise = new Promise((resolve, reject) => {
      that.request(options)
        .then((res) => {
          if (res.statusCode === 200)
            resolve(res.data);
          else
            reject(res);
        }, (err) => {
          reject(err);
        });
    });

    return promise;
  }


  /**
   * group payment
   * @param {Array <String>} include
   * @param {Object} filter
   * @param {Array <Object>} fields
   */
  payment ({include = undefined, filter = undefined, fields = undefined, method = 'GET', _pouchdb = null, dbname = ''}) {
    let that = this;
    let options = {
      host: that.host,
      // host: 'pmt.sparkpad-dev.com',
      method: method,
      protocol: that.protocol,
      headers: {
        'Content-Type': 'application/vnd.api+json',
        'Authorization': that.auth
      },
      port: that.port
    };
    let path = '/jsonapi/node/account';
    let promise = new Promise((resolve, reject) => {
      that.find({options, path, include, filter, fields})
        .then((_data) => {
          if (_data.statusCode === 200) {
            try {
              let data = JSON.parse(_data.data);
              // data 数据为null 直接 返回data 由调用者处理
              if (data === null)
                reject(data);
              // 处理 存储到数据库的信息
              let doc = {
                _id: data.data[0].attributes.uuid, // 服务器上存储的商家id 作为pouchdb _id
                nid: data.data[0].attributes.nid, // 服务器上存储的商家id
                uuid: data.data[0].attributes.uuid, // 服务器上存储的商家uuid
                vid: data.data[0].attributes.vid,
                langcode: data.data[0].attributes.langcode,
                status: data.data[0].attributes.status,
                title: data.data[0].attributes.title,
                created: data.data[0].attributes.created,
                changed: data.data[0].attributes.changed,
                promote: data.data[0].attributes.promote,
                sticky: data.data[0].attributes.sticky,
                revision_timestamp: data.data[0].attributes.revision_timestamp,
                revision_log: data.data[0].attributes.revision_log,
                revision_translation_affected: data.data[0].attributes.revision_translation_affected,
                default_langcode: data.data[0].attributes.default_langcode,
                path: data.data[0].attributes.path,
                field_alipay_app_id: data.data[0].attributes.field_alipay_app_id,
                field_alipay_sandbox_app_id: data.data[0].attributes.field_alipay_sandbox_app_id,
                field_de_business_id: data.data[0].attributes.field_de_business_id,
                field_vbpay_iv: data.data[0].attributes.field_vbpay_iv,
                field_vbpay_mno: data.data[0].attributes.field_vbpay_mno,
                field_vbpay_partnerid: data.data[0].attributes.field_vbpay_partnerid,
                field_vbpay_sandbox_iv: data.data[0].attributes.field_vbpay_sandbox_iv,
                field_vbpay_sandbox_mno: data.data[0].attributes.field_vbpay_sandbox_mno,
                field_vbpay_sandbox_partnerid: data.data[0].attributes.field_vbpay_sandbox_partnerid,
                field_wechat_appid: data.data[0].attributes.field_wechat_appid,
                field_wechat_key: data.data[0].attributes.field_wechat_key,
                field_wechat_mch_id: data.data[0].attributes.field_wechat_mch_id,
                field_wechat_sub_appid: data.data[0].attributes.field_wechat_sub_appid,
                field_wechat_sub_mch_id: data.data[0].attributes.field_wechat_sub_mch_id
              };
              if (_pouchdb !== null) {
                // 销毁数据库原有数据
                let db = _pouchdb.createDatabase(dbname);
                _pouchdb.destroy(db).then(res => {
                  db = _pouchdb.createDatabase(dbname);
                  _pouchdb.put(db, doc).then(res => {
                    resolve(res);
                  }, err => {
                    reject(err);
                  });
                }, err => {
                  reject(err);
                });
              }
            } catch (error) {
              reject(error);
            }
          } else {
            // 非 200 直接返回_data 由调用者处理
            reject(_data);
          }
        }, (err) => {
          reject(err);
        });
    });
    return promise;
  }
};